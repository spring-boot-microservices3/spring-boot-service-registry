## SPRING BOOT SERVICE REGISTRY

# requirement ENV
- java 11

```
├── README.md
├── Dockerfile
├── pom.xml
└── src
    ├── main
    │   ├── java
    │   │   └── spring.service.registry
    │   ├── resources
    │   │   └── application.properties
```
if you need custom port, change on `application.properties` 
```
server.port=8761
eureka.client.register-with-eureka=false
eureka.client.fetch-registry=false

logging.level.com.netflix.eureka=OFF
logging.level.com.netflix.discovery=OFF

```

and run the services
```aspectj
mvn springb-boot:run
```


